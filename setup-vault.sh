#!/bin/sh

set -e

echo 'Creating namespace'
vault namespace create gitlab
export VAULT_NAMESPACE=gitlab

echo 'Enabling kv backend'
vault secrets enable -version=2 kv
vault kv put kv/example mysecret=somesecretvalue

echo 'Enabling jwt backend'
vault auth enable jwt
echo 'Creating ACL policy'
vault policy write example - <<EOF
path "kv/data/example" {
  capabilities = [ "read" ]
}
EOF

echo 'Creating jwt role'
vault write auth/jwt/role/example - <<EOF
{
  "role_type": "jwt",
  "policies": ["example"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims": {
    "iss": "gitlab.com"
  }
}
EOF

echo 'Writing JWT config'
vault write auth/jwt/config \
  jwks_url="https://gitlab.com/-/jwks" \
  bound_issuer="gitlab.com"
